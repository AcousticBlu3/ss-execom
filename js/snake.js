/////////////////////////////////////////////////////////////
    
    // Canvas & Context
    var canvas;
    var ctx;
    
    // Snake
    var snake;
    var snake_dir;
    var snake_next_dir;
    var snake_speed;
    var gameover;
    
    
    // Food
    var food = {x: 0, y: 0};
    
    // Score
    var score;
    // Setting the default high score to 0 when the page refreshes
    var highScore=0;
    // Wall
    var wall;
    var wallGO;
    
    
    // HTML Elements
    var screen_snake;
    var screen_menu;
    var screen_gameover;
    var button_newgame_menu;
    var button_newgame_gameover;
    var ele_score;
    var ele_highScore;
    var speed_setting;
    var wall_setting;
    var wallGO_setting;
    
    
    /////////////////////////////////////////////////////////////

    var activeDot = function(x, y){
        ctx.fillStyle = "#0000FF";
        ctx.fillRect(x * 10, y * 10, 10, 10);
    }
    
    
    /////////////////////////////////////////////////////////////

    var changeDir = function(key){
       
        if(key == 38 && snake_dir != 2){
            snake_next_dir = 0;
        }else{
        
        if (key == 39 && snake_dir != 3){
            snake_next_dir = 1;
        }else{
        
        if (key == 40 && snake_dir != 0){
            snake_next_dir = 2;
        }else{
            
        if(key == 37 && snake_dir != 1){
            snake_next_dir = 3;
        } } } }
        
    }
    
    /////////////////////////////////////////////////////////////

    var addFood = function(){
        food.x = Math.floor(Math.random() * ((canvas.width / 10) - 1));
        food.y = Math.floor(Math.random() * ((canvas.height / 10) - 1));
        for(var i = 0; i < snake.length; i++){
            if(checkBlock(food.x, food.y, snake[i].x, snake[i].y)){
                addFood();
            }
        }
    }
    
    /////////////////////////////////////////////////////////////

    var checkBlock = function(x, y, _x, _y){
        return (x == _x && y == _y) ? true : false;
    }
    
    ///////////////////////////////////////////////////////////// 
    
    var altScore = function(score_val){
        ele_score.innerHTML = String(score_val);
        
    }
    var altHighScore = function(highScore_val){
        ele_highScore.innerHTML = String(highScore_val);
        
    }
    
    /////////////////////////////////////////////////////////////

    var mainLoop = function(){
        
            var _x = snake[0].x;
            var _y = snake[0].y;
      snake_dir = snake_next_dir;

            // 0 - Up, 1 - Right, 2 - Down, 3 - Left
            switch(snake_dir){
                case 0: _y--; break;
                case 1: _x++; break;
                case 2: _y++; break;
                case 3: _x--; break;
            }

            snake.pop();
            snake.unshift({x: _x, y: _y});
            
            //provera koordinata glave zmije
            /*console.log(_x);
            console.log(_y);*/
        
        // --------------------

        // Wall
        
            if(wall == 1){
                // setting the checked button on GameOver screen
                document.getElementById("wallon1").checked = true;
            // On
                if (snake[0].x < 0 || snake[0].x == canvas.width / 10 || snake[0].y < 0 || snake[0].y == canvas.height / 10){
                    
                    showScreen(2);
                    gameover=true;
                    

                    
                    if(score>highScore){  ///changing highScore if the current one is bigger
                        highScore=score;
                        altHighScore(highScore);
                    }
                    console.log("smrt")
                    console.log(document.getElementById('myRange').value); 
                    console.log(document.getElementById('myRange2').value); 
                    
                    return;
                }
            }else{
                // setting the checked button on GameOver screen
                document.getElementById("walloff1").checked = true;
            // Off
                for(var i = 0, x = snake.length; i < x; i++){
                    if(snake[i].x < 0){
                        snake[i].x = snake[i].x + (canvas.width / 10);
                    }
                    if(snake[i].x == canvas.width / 10){
                        snake[i].x = snake[i].x - (canvas.width / 10);
                    }
                    if(snake[i].y < 0){
                        snake[i].y = snake[i].y + (canvas.height / 10);
                    }
                    if(snake[i].y == canvas.height / 10){
                        snake[i].y = snake[i].y - (canvas.height / 10);
                    }
                }
            }
        
        // --------------------
    
        // Autophagy death
            for(var i = 1; i < snake.length; i++){
                if (snake[0].x == snake[i].x && snake[0].y == snake[i].y){
                    
                    showScreen(2);
                    gameover=true;
                    
                    
                    if(score>highScore){  ///changing highScore if the current one is bigger
                        highScore=score;
                        altHighScore(highScore);
                    }
                        
                    
                    return;
                    console.log("smrt")
                    console.log(document.getElementById('myRange').value); 
                    console.log(document.getElementById('myRange2').value); 
                    document.getElementById('myRange2').value=document.getElementById("myRange2").defaultValue;
                    document.getElementById('myRange').value=document.getElementById("myRange").defaultValue;
                   
                }
                
            }
            
      
        // --------------------
        
      // Eat Food
            if(checkBlock(snake[0].x, snake[0].y, food.x, food.y)){
                snake[snake.length] = {x: snake[0].x, y: snake[0].y};
                score += 1;
                altScore(score);
                
                addFood();
                activeDot(food.x, food.y);
            }
        
        // --------------------

            ctx.beginPath();
            ctx.fillStyle = "#000000";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
        
        // --------------------

            for(var i = 0; i < snake.length; i++){
                activeDot(snake[i].x, snake[i].y);
            }
        
        // --------------------

            activeDot(food.x, food.y);
        
    // Debug
    //document.getElementById("debug").innerHTML = snake_dir + " " + snake_next_dir + " " + snake[0].x + " " + snake[0].y;    

            setTimeout(mainLoop, snake_speed);
    }
    
    /////////////////////////////////////////////////////////////
    //change myRange value to the one of myRange2 or vice versa depending on the screen (main or gameover screen)
    var newGame = function(){
        
        showScreen(0);
        screen_snake.focus();
        console.log("Nova partija")
        if(gameover==true){
            document.getElementById("myRange").value=document.getElementById("myRange2").value;
        }
        else{
        document.getElementById("myRange2").value=document.getElementById("myRange").value;
        }   
        console.log(document.getElementById('myRange').value); 
        console.log(document.getElementById('myRange2').value);  
        
      
        snake = [];
        for(var i = 4; i >= 0; i--){
            snake.push({x: i, y: 15});
        }
      
        snake_next_dir = 1;
        
        score = 0;
        
        altScore(score);
        
        
        addFood();
        
        document.onkeydown = function(evt) {
            evt = evt|| window.evt;
            changeDir(evt.keyCode);
        }
        mainLoop();
                
    }
    
    /////////////////////////////////////////////////////////////
    
    // Change the snake speed...
    // 150 = slow
    // 100 = normal
    // 50 = fast
    var setSnakeSpeed = function(speed_value){
        snake_speed = speed_value;
    }
    
    /////////////////////////////////////////////////////////////
    var setWall = function(wall_value){
        wall = wall_value;
        if(wall == 0){screen_snake.style.borderColor = "#606060";}
        if(wall == 1){screen_snake.style.borderColor = "#FFFFFF";}
    }
    var setWallGO = function(wallGO_value){
        wallGO = wallGO_value;
        if(wallGO == 0){screen_snake.style.borderColor = "#606060";}
        if(wallGO == 1){screen_snake.style.borderColor = "#FFFFFF";}
    }
    //gameover WALL option
    
    /////////////////////////////////////////////////////////////
    
    // 0 for the game
    // 1 for the main menu
    // 2 for the game over screen
    var showScreen = function(screen_opt){
        switch(screen_opt){
                
            case 0:  screen_snake.style.display = "block";
                     screen_menu.style.display = "none";
                     screen_gameover.style.display = "none";
                     
                     break;
                
            case 1:  screen_snake.style.display = "none";
                     screen_menu.style.display = "block";
                     screen_gameover.style.display = "none";
                     
                     break;
                
           
            case 2: screen_snake.style.display = "none";
                    screen_menu.style.display = "none";
                    screen_gameover.style.display = "block";
                    
                    break;
        }
    }
        
    /////////////////////////////////////////////////////////////
     // values of myRange and myRange2 are logged for the purposes of debugging   
    window.onload = function(){
        
        canvas = document.getElementById("snake");
        ctx = canvas.getContext("2d");
        
        console.log("prvo ucitavanje")
        console.log(document.getElementById('myRange').value);  
        console.log(document.getElementById('myRange2').value);  
            // Screens
            screen_snake = document.getElementById("snake");
            screen_menu = document.getElementById("menu");
            screen_gameover = document.getElementById("gameover");
            
        
            // Buttons
            button_newgame_menu = document.getElementById("newgame_menu");
            button_newgame_gameover = document.getElementById("newgame_gameover");
            
        
        
            // etc
            ele_score = document.getElementById("score_value");
            ele_highScore = document.getElementById("highScore_value");
            speed_setting = document.getElementById("myRange");
            wall_setting = document.getElementsByName("wall");
            wallGO_setting = document.getElementsByName("wallGO");
            
            
            
        
        // --------------------

        button_newgame_menu.onclick = function(){newGame();};
        button_newgame_gameover.onclick = function(){newGame();}; 
        

        setSnakeSpeed(100);
        setWall(1);
        
        
        
        
        

        showScreen("menu");
        
        // --------------------
        // Settings
        // values of myRange and myRange2 are logged for the purposes of debugging
            // speed
            var slider = document.getElementById('myRange');
            slider.addEventListener('input', sliderChangeM);
            
            function sliderChangeM() {
                console.log(document.getElementById('myRange').value);
                console.log("Change of slider");
                if (document.getElementById('myRange').value==1){
                    setSnakeSpeed(150);
                 }else if(document.getElementById('myRange').value==2){
                     setSnakeSpeed(100);
                 }else if (document.getElementById('myRange').value==3){
                    setSnakeSpeed(50);
                }    
            }
                          
            var slider = document.getElementById('myRange2');
            slider.addEventListener('input', sliderChangeGO);
            //slider change on gameover screen
            function sliderChangeGO() {
                console.log(document.getElementById('myRange2').value);
                console.log("Change of slider");
                if (document.getElementById('myRange2').value==1){
                    setSnakeSpeed(150);
                 }else if(document.getElementById('myRange2').value==2){
                     setSnakeSpeed(100);
                 }else if (document.getElementById('myRange2').value==3){
                    setSnakeSpeed(50);
                }   
            }
                  
           
            // wall
            for(var i = 0; i < wall_setting.length; i++){
                wall_setting[i].addEventListener("click", function(){
                    for(var i = 0; i < wall_setting.length; i++){
                        if(wall_setting[i].checked){
                            setWall(wall_setting[i].value);
                            setWallGO(wall_setting[i].value);
                        }
                    }
                });
            }
            for(var i = 0; i < wallGO_setting.length; i++){
                wallGO_setting[i].addEventListener("click", function(){
                    for(var i = 0; i < wallGO_setting.length; i++){
                        if(wallGO_setting[i].checked){
                            setWallGO(wallGO_setting[i].value);
                            setWall(wallGO_setting[i].value);
                        }
                    }
                });
            }
            //different keydown listener
            document.addEventListener('keydown', space);

            function space(e) {
                if(screen_gameover.style.display == "block"){
                    e = e || window.event;
                if(e.keyCode==32){
                    console.log(e.keyCode);
                    newGame();
                }
            }
        }
    }
