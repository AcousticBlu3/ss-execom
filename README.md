# SS-Snake game
- Ultra Snake is a remake of a famous retro game called Snake.
- The snake grows longer by eating food. 
- The score increases with each piece of food eaten. 
- The game session ends when the snake tries to eat its own body
or collides with a wall (if the wall option is turned on). 
- The game also keeps track of player's high score during a session.
## Technologies
- HTML5 Canvas
- JavaScript
## Installation and prerequisites
- The only prerequisite is having the latest version of Google Chrome.
- The game has also been tested on Microsoft Edge and it works there just as well.
## Usage
1. Launch index.html
2. Select one of the 3 available game speeds from the slider given
3. Toggle the wall on/off
4. Start the game
5. Get as many points as possible by eating more food and avoiding death
6. If the snake collides with its own body or wall, the game over screen is shown.
7. We can choose a different speed and/or change the wall setting before starting again.
8. Enjoy the game!
## Contributing
1. Create your feature branch: `git checkout -b my-new-feature`
2. Commit your changes: `git commit -am 'Add some feature'`
3. Push to the branch: `git push origin my-new-feature`
4. Submit a pull request

## Credits
- Creator: Srdjan Sretenovic aka AcousticBlu3 (GitLab username)
